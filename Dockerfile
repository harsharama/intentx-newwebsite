From node:latest

#RUN npm install -g yarn

WORKDIR /usr/src/app

COPY . .

RUN yarn install

EXPOSE 3000

CMD ["yarn", "start"]
