import React from 'react';
// import { ThemeContext } from '../../utils/theme';
import Footer from '../../components/common/Footer';

import Header from '../../components/common/Header';
// import waves1 from '../../staticassets/solutions/first/background/backgroundwaves1@2x.png';
import "./index.css"
import FirstSection from '../../components/solutions/FirstSection';
import SecoundSection from '../../components/solutions/SecoundSection';
import ThirdSection from '../../components/solutions/ThirdSection';

const Publishers = () => {
  return (
    <>
<Header />
{/* <div className="topimg solu"><img src={waves1} alt="waves1" /></div> */}
    <div className="solution">
    <FirstSection />

    <SecoundSection />
    <ThirdSection />
    </div>
<Footer />

    </>
  );
}

export default Publishers;