import React from 'react';
import "./index.css"
// import adspendinggraphic from '../../../staticassets/publisher/third/graph/AffiliateSpendinginBilliongraph@2x.png';
// import adrevenues from '../../../staticassets/publisher/secound/icons/adrevenues.png';
// import backgroundwaves from '../../../../src/staticassets/publisher/sixth/background/backgroundwaves4@2x.png';
import publishers from '../../../../src/staticassets/publisher/sixth/publishers@2x.png';

const SecoundSection = () => {
  return (
      <>
<section className="pub-sixth">
<div className="container">
<div className="row">
  {/* <div className="sixth-head">
      <h1>Trusted Partner of <b>Leading Publishers</b></h1>
  </div > */}
  <div className="trending-carousal">
  <img src={publishers} alt="yellowwaves"style={{ width: '100%'}} />
  </div>
  </div>

</div>
</section>
</>
  );
}

export default SecoundSection;