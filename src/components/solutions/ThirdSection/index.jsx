import React from 'react';
import "./index.css"
// import adspendinggraphic from '../../../staticassets/publisher/third/graph/AffiliateSpendinginBilliongraph@2x.png';
// import adrevenues from '../../../staticassets/publisher/secound/icons/adrevenues.png';
// import backgroundwaves3 from '../../../../src/staticassets/solutions/third/background/backgroundwaves3@2x.png';
import cashXimage from '../../../../src/staticassets/solutions/third/image/cashXimage@2x.png';
import FourthSection from '../FourthSection';

const SecoundSection = () => {
  return (
      <>
<section className="sol-third">
  <div className="svgflip2" style={{ marginBottom: '-5px', width: '100%'}}>
<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1920 185.978">
  <path id="Trazado_2013" data-name="Trazado 2013" d="M-1.489,24.244s86.441-7.222,219.764,1.533S811.592,150.87,1030.661,129.387s537.776-76.824,706.595-83.079,181.253-8.228,181.253-8.228V208h-1920Z" transform="translate(1.491 -21.445)" fill="#e9c9de"/>
  <path id="Trazado_2012" data-name="Trazado 2012" d="M-1.491.035s102.9-1.411,349.288,25.12S726.811,76.594,992.1,76.594s368.48-31.787,625.261-45.892,301.144-15.315,301.144-15.315V166h-1920Z" transform="translate(1.491 22.12)" fill="#f01ea1"/>
</svg>
</div>
{/* <div className="massive-ywaves1"><img src={backgroundwaves3} alt="yellowwaves" style={{ width: '100%'}} /></div> */}
<div className="cashxbck">
<div className="container">
<div className="row">
  <div className="col-12  third-left">
      <h2>cash<sup>X</sup></h2>
        <p>Coupon-automating and cash-back browser extension provides high-intent, first-party integrated data.
</p>
  </div >
  <div className="col-8 third-right"><div className="topimage"><img src={cashXimage} alt="firstsection" style={{ width: '100%'}} /></div></div>
  </div>

</div>
</div>
<div className="svgflip2">
<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1920 185.978">
<path id="Trazado_2092" data-name="Trazado 2092" d="M1.714,800.512h1920V904.029S1819.154,841.5,1638.292,863s-550.01,38.218-734.472,14.241S542.035,868.779,299.531,863,1.714,851.845,1.714,851.845Z" transform="translate(-1.714 -800.512)" fill="#efb5da" opacity="0.546"/>
<path id="Trazado_2093" data-name="Trazado 2093" d="M1.714,758.512h1920V865.481s-168.359-94.673-330.8-68.536-315.44,49.113-620.781,22.98-349.989,39.763-574.135,0S1.714,796.945,1.714,796.945Z" transform="translate(-1.714 -758.512)" fill="#fb59be" opacity="0.998"/>
</svg>
</div>
</section>
<div className="sol-thirdreport">
<FourthSection />
</div>

</>
  );
}

export default SecoundSection;