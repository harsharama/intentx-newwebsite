import React from 'react';
import "./index.css"
// import adspendinggraphic from '../../../staticassets/publisher/third/graph/AffiliateSpendinginBilliongraph@2x.png';
// import adrevenues from '../../../staticassets/publisher/secound/icons/adrevenues.png';
import laptopdashboard from '../../../../src/staticassets/publisher/sixth/gif-dashboard.gif';
// import backgroundwaves4 from '../../../../src/staticassets/solutions/fourth/background/backgrundwaves4@2x.png';


const FourthSection = () => {
  return (
      <>
<section className="sol-fourth">
{/* <div className="massive-ywaves1"><img src={backgroundwaves4} alt="yellowwaves" style={{ width: '100%'}} /></div> */}
<div className="container">
<div className="row sol-solrow">
  <div className="col-4  fourth-left">
      <h1>Reporting</h1>
        <p>Reliable data tracking and real-time performance metrics provide full visibility and helpful insights. It’s your data—use it however you like.
</p>
  </div >
  <div className="col-8 fourth-right"><div className="topimage"><img src={laptopdashboard} alt="firstsection" style={{ width: '100%'}} /></div></div>
  </div>

</div>
</section>
</>
  );
}

export default FourthSection;