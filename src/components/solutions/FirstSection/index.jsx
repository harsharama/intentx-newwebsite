import React from 'react';
import "./index.css"
import firstsectionimg from '../../../../src/staticassets/solutions/first/image/OneStopShop_ipad@2x.png';

const Home = () => {
  return (
      <>
<section className="topsection1">
<article className="bannersection">
<div className="row">
  <div className="col-12  one">
<div className="container">

      <h1>Leading Affiliate Commerce Platform <b> For Publishers.</b></h1>
        <p>One Stop Shop For Publishers</p>
  </div >

</div>
<div className="svgflip2" style={{ marginTop: '-5px', width: '100%'}}>
<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1920 185.978">
<path id="Trazado_2022" data-name="Trazado 2022" d="M.111,24s84.841-6.978,218.163,1.777S811.592,150.87,1030.661,129.387s537.776-76.824,706.595-83.079S1920.111,38,1920.111,38V239.845H.111Z" transform="translate(1920.111 239.845) rotate(180)" fill="#dae6f4"/>
<path id="Trazado_2023" data-name="Trazado 2023" d="M.111,0S101.411-1.375,347.8,25.155,726.811,76.594,992.1,76.594s368.48-31.787,625.261-45.892,302.746-15.441,302.746-15.441V197.845H.111Z" transform="translate(1920.111 197.845) rotate(180)" fill="#305ab9"/>
</svg>
</div>
  <div className="soltwo">
<div className="container">
    <div className="topimage"><img src={firstsectionimg} alt="firstsection" /></div>
<p className="toptxt">Immediate scale and seamless integration. intent<sup>X</sup> does most of the work.</p>
  </div>
  </div>
  </div>
</article>
</section>
</>
  );
}

export default Home;