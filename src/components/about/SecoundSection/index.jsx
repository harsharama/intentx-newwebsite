import React from 'react';
import "./index.css"
// import backgroundwaves from '../../../../src/staticassets/about/third/background/backgroundwaves2@2x.png';
// import hilary from '../../../../src/staticassets/about/third/teamphotos/hilary.png';
import molly from '../../../../src/staticassets/about/third/teamphotos/molly_francis.jpg';
import mom from '../../../../src/staticassets/about/third/teamphotos/mom_chan.jpg';
import ruben from '../../../../src/staticassets/about/third/teamphotos/ruben_baerga.jpg';
import samy from '../../../../src/staticassets/about/third/teamphotos/samuel_baez.jpg';
// import tommy from '../../../../src/staticassets/about/third/teamphotos/tommy.png';
import vada from '../../../../src/staticassets/about/third/teamphotos/vada_abrahamson.jpg';
import vamsi from '../../../../src/staticassets/about/third/teamphotos/vamsi_putrevu.jpg';
import sudharshan from '../../../../src/staticassets/about/third/teamphotos/Sudharshan_Srinivasan.jpg';
import harsha from '../../../../src/staticassets/about/third/teamphotos/HarshaMR.jpg';
import bhoomi from '../../../../src/staticassets/about/third/teamphotos/bhoomi_parmar.jpg';
import deepika from '../../../../src/staticassets/about/third/teamphotos/deepika_verma.jpg';
import devayani from '../../../../src/staticassets/about/third/teamphotos/devayani_bapat.jpg';
import men from '../../../../src/staticassets/about/third/teamphotos/Men_avatar.jpg';
import women from '../../../../src/staticassets/about/third/teamphotos/Women_avatar.jpg';
// import marketplaceX from '../../../../src/staticassets/solutions/second/images/marketplaceX@2x.png';

const SecoundSection = () => {
  return (
      <>
<section className="abt-secound">
<div className="svgflip2" style={{ width: '100%'}}>
<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1920 205">
<path id="Trazado_2009" data-name="Trazado 2009" d="M0,934.5H1920V973s-215.341-4.4-398.038,20.232-368.227-38.821-589.2,42.6S601.733,1063.579,396.4,1086.3,0,1169.5,0,1169.5Z" transform="translate(1920 1139.502) rotate(180)" fill="#fff6d1"/>
<path id="Trazado_2008" data-name="Trazado 2008" d="M0,982.5H1920V993s-215.341-4.4-398.038,20.232-368.227-38.821-589.2,42.6S601.733,1082.579,396.4,1105.3,0,1187,0,1187Z" transform="translate(1920 1187) rotate(180)" fill="#ffe992"/>

</svg>
</div>
{/* <div className="massive-ywaves1"><img src={backgroundwaves} alt="yellowwaves"style={{ width: '100%'}} /></div> */}
<div className="abt-secound-bck">

<div className="container">
<div className="abt-third-head">
<h2>The Team</h2>
</div>
<div className="teamphots">
    <ul>
        <li>
        <div className="outer"><img src={vamsi} alt="vamsi"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Vamsi Putrevu</b> <span>Co-Founder</span></p></div>
        </li>
        <li>
        <div className="outer"><img src={mom} alt="mom"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Mom Chan</b> <span>Co-Founder</span></p></div>
        </li>
        <li>
        <div className="outer"><img src={molly} alt="molly"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Molly Francis</b> <span>Data Engineering &amp; QA</span></p></div>
        </li>
        <li>
        <div className="outer"><img src={samy} alt="samy"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Samuel Baez</b> <span>Designer</span></p></div>
        </li>

        <li>
        <div className="outer"><img src={ruben} alt="ruben"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Ruben Baerga</b> <span>Engineer</span></p></div>
        </li>

        <li>
        <div className="outer"><img src={deepika} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Deepika Verma</b> <span>Engineer</span></p></div>
        </li>

        <li>
        <div className="outer"><img src={women} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Molly Wolfe</b> <span>Content Editor</span></p></div>
        </li>

        {/* <li>
        <div className="outer"><img src={tommy} alt="tommy"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Tommy So</b> <span>Engineer</span></p></div>
        </li> */}
        {/* <li>
        <div className="outer"><img src={hilary} alt="hilary"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Hilary Braaksma</b> <span>Head of Content</span></p></div>
        </li> */}
        <li>
        <div className="outer"><img src={vada} alt="vada"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Vada Abrahamson</b> <span>Content Editor</span></p></div>
        </li>
        <li>
        <div className="outer"><img src={devayani} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Devayani Bapat</b> <span>Content Editor</span></p></div>
        </li>

        <li>
        <div className="outer"><img src={harsha} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Harsha Mr</b> <span>UI Engineer</span></p></div>
        </li>

        <li>
        <div className="outer"><img src={sudharshan} alt="ruben"style={{ width: '100%'}} />

        </div>
        <div className="inner"><p><b>Sudharshan Srinivasan</b> <span>Engineer</span></p></div>
        </li>
        <li>
        <div className="outer"><img src={men} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Dhiren Upadhyay</b> <span>Engineer</span></p></div>
        </li>

        <li>
        <div className="outer"><img src={women} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Gowthami</b> <span>Engineer</span></p></div>
        </li>
        <li>
        <div className="outer"><img src={men} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Soumeet Acharya</b> <span>Engineer</span></p></div>
        </li>
        <li>
        <div className="outer"><img src={bhoomi} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Bhoomi Parmar</b> <span>Engineer</span></p></div>
        </li>



        <li>
        <div className="outer"><img src={men} alt="harsha"style={{ width: '100%'}} />
        </div>
        <div className="inner"><p><b>Nisarg Pandya</b> <span>Engineer</span></p></div>
        </li>

        </ul>
    </div>

</div>
</div>
</section>
</>
  );
}

export default SecoundSection;