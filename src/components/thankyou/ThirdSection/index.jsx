import React from 'react';
import "./index.css"
// import adspendinggraphic from '../../../staticassets/publisher/third/graph/AffiliateSpendinginBilliongraph@2x.png';
// import adrevenues from '../../../staticassets/publisher/secound/icons/adrevenues.png';
import backgroundwaves3 from '../../../../src/staticassets/solutions/third/background/backgroundwaves3@2x.png';
import cashXimage from '../../../../src/staticassets/solutions/third/image/cashXimage@2x.png';

const SecoundSection = () => {
  return (
      <>
<section className="sol-third">
<div className="massive-ywaves1"><img src={backgroundwaves3} alt="yellowwaves" style={{ width: '100%'}} /></div>
<div className="container">
<div className="row">
  <div className="col-12  third-left">
      <h2>cashX</h2>
        <p>Coupon-automating and cash-back browser extension provides high-intent, first-party integrated data.
</p>
  </div >
  <div className="col-8 third-right"><div className="topimage"><img src={cashXimage} alt="firstsection" style={{ width: '100%'}} /></div></div>
  </div>

</div>
</section>
<div className="sol-thirdreport">
</div>

</>
  );
}

export default SecoundSection;