import React from 'react';
import { NavLink } from 'react-router-dom';

import './index.css';
// import backgroundwave from '../../../staticassets/Bottombar/background/backgroundwave2x.png';
// import facebook from '../../../staticassets/Bottombar/socialmedia/facebook.png';
// import instagram from '../../../staticassets/Bottombar/socialmedia/instagram.png';
// import twitter from '../../../staticassets/Bottombar/socialmedia/twitter.png';
const Footer = ({btnName}) => {

  return (

         <div className="footer clearfix" >
<div className="footerLinks">

    <ul>
        <li><NavLink
                exact
                to="/publishers"
                activeClassName="active"
                className="nav-links"
                // onClick={click ? handleClick : null}
              >
 Publishers
              </NavLink>
              </li>
        <li><NavLink
                exact
                to="/advertisers"
                activeClassName="active"
                className="nav-links"
                // onClick={click ? handleClick : null}
              >
 Advertisers
              </NavLink>
              </li>
        <li><NavLink
                exact
                to="/solutions"
                activeClassName="active"
                className="nav-links"
                // onClick={click ? handleClick : null}
              >
 Solutions
              </NavLink>
              </li>
        <li><NavLink
                exact
                to="/about"
                activeClassName="active"
                className="nav-links"
                // onClick={click ? handleClick : null}
              >
 About Us
              </NavLink>
              </li>
        <li><NavLink
                exact
                to="/terms-of-service"
                activeClassName="active"
                className="nav-links"
                // onClick={click ? handleClick : null}
              >
Terms of service
              </NavLink>
              </li>
        <li><NavLink
                exact
                to="/privacy-policy"
                activeClassName="active"
                className="nav-links"
                // onClick={click ? handleClick : null}
              >
 Privacy Policy
              </NavLink>
              </li>
        <li><NavLink
                exact
                to="/website-terms-of-use"
                activeClassName="active"
                className="nav-links"
                // onClick={click ? handleClick : null}
              >
Website Terms
              </NavLink>
              </li>
    </ul>
</div>

             <div className="footer-bckimg">
             {/* <svg xmlns="http://www.w3.org/2000/svg" width="1920" height="155.012" viewBox="0 0 1920 155.012">
  <path id="Trazado_2021" data-name="Trazado 2021" d="M0,24s84.952-6.978,218.274,1.777S811.592,150.87,1030.661,129.387s537.776-76.824,706.595-83.079S1920,38,1920,38V197H0Z" transform="translate(0 -21.365)" fill="#d9ebf6"/>
  <path id="Trazado_2020" data-name="Trazado 2020" d="M0,0S101.411-1.375,347.8,25.155,726.811,76.594,992.1,76.594s368.48-31.787,625.261-45.892S1920,15.262,1920,15.262V155H0Z" transform="translate(0 0.012)" fill="#305ab9"/>

</svg> */}
<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 155.01"><defs></defs>
<path class="cls-1" d="M-8,240.72s85-7,218.27,1.78,593.32,125.09,812.39,103.61,537.78-76.82,706.6-83.08S1912,254.72,1912,254.72v159H-8Z" transform="translate(8 -238.09)" fill="#d9ebf6" />
<path class="cls-1" d="M-228.42,907.15s101.41-1.37,347.8,25.16,379,51.44,644.3,51.44,368.48-31.79,625.26-45.9,302.64-15.44,302.64-15.44v139.74h-1920Z" transform="translate(228.42 -893.14)" fill='#305ab9'/>
</svg>
</div>
<div className="copyandsocial">
    <div className="copyright">copyright 2022 intentX. All rights reserved</div>
</div>

    </div>
  );
}

export default Footer;