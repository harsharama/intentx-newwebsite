import React from 'react';
import "./index.css"

const FirstSection = () => {
  return (
      <>
<section className="topsection">
<div className="container">

<article className="bannersection">
<div className="row">
  <div className="col-12  one">
      <h1> Contact Us</h1>
  </div >
  </div>
</article>
</div>
</section>
<div className="svgflip2" style={{ width: '100%'}}>
<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1920 105">
<path id="Trazado_2028" data-name="Trazado 2028" d="M0,516H1920V621s-53.051-.674-177.238-35.939S1574.284,607.242,1310.639,604s-420.3-39.526-622.457-31.907C509.89,578.812,371.24,620.307,212.189,618,41.156,615.7,0,609,0,609Z" transform="translate(0 -516)" fill="#dae6f4"/>
<path id="Trazado_2029" data-name="Trazado 2029" d="M0,536H1920v65s-157.287,18.865-322.834-9.162-372.588-25.271-741.615,20.689S0,579,0,579Z" transform="translate(0 -536)" fill="#305ab9"/>

</svg>
</div>
</>
  );
}

export default FirstSection;