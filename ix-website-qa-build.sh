#!/bin/bash

git reset --hard || exit 1
git fetch -p || exit 1
git checkout $1 || exit 1
git pull origin $1 || exit 1
docker build -t intentx-website . || exit 1;
docker rm -f intentx-website;
docker run --name intentx-website -d -p 3001:3000 intentx-website:latest